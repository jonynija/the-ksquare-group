import 'package:flutter/material.dart';

import 'package:users/data_source/models/user.dart';

class UserItem extends StatelessWidget {
  const UserItem({
    super.key,
    required this.user,
    required this.onTap,
  });

  final User user;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        onTap: onTap,
        leading: const Icon(Icons.person),
        title: Text(
          '${user.name}. AKA: ${user.userName}',
          maxLines: 1,
        ),
        subtitle: Text(
          user.email,
        ),
      ),
    );
  }
}
