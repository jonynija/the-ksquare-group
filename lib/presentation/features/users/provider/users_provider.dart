import 'package:flutter/material.dart';

import 'package:delightful_toast/delight_toast.dart';
import 'package:delightful_toast/toast/components/toast_card.dart';

import 'package:users/data_source/models/user.dart';
import 'package:users/data_source/repository/user_repository.dart';

class UsersProvider extends ChangeNotifier {
  UsersProvider({
    required UserRepository repository,
  }) : _repository = repository;

  final UserRepository _repository;

  List<User>? users;

  void init() async {
    final result = await _repository.getUsers();

    await Future.delayed(const Duration(seconds: 1));

    users = result?.users;
    notifyListeners();
  }

  void onUserTap(BuildContext context, User user) {
    DelightToastBar(
      builder: (context) => ToastCard(
        leading: const Icon(
          Icons.flutter_dash,
          size: 28,
        ),
        title: Text(
          "Hi I'm Dash, ${user.email}",
          style: const TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 14,
          ),
        ),
      ),
      autoDismiss: true,
    ).show(context);
  }
}
