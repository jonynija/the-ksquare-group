import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;

class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        return macos;
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyCUYNXr4Hbq0PmgdA0XAUIx7jad8C4Jk-w',
    appId: '1:912516069775:web:42929dd28a0534b9dfb3fa',
    messagingSenderId: '912516069775',
    projectId: 'the-ksquare-group',
    authDomain: 'the-ksquare-group.firebaseapp.com',
    storageBucket: 'the-ksquare-group.appspot.com',
    measurementId: 'G-98P1B69VS2',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyDeJB1o4lHoLCBE508pxW3zhotY-7Ualfk',
    appId: '1:912516069775:android:8a2dbf8ea03792c6dfb3fa',
    messagingSenderId: '912516069775',
    projectId: 'the-ksquare-group',
    storageBucket: 'the-ksquare-group.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyB9V41eC8QbNeFWfsHLNEU70bHSKCmxKYQ',
    appId: '1:912516069775:ios:b3da447bc1186c39dfb3fa',
    messagingSenderId: '912516069775',
    projectId: 'the-ksquare-group',
    storageBucket: 'the-ksquare-group.appspot.com',
    iosBundleId: 'com.jixcayau.ksquare',
  );

  static const FirebaseOptions macos = FirebaseOptions(
    apiKey: 'AIzaSyB9V41eC8QbNeFWfsHLNEU70bHSKCmxKYQ',
    appId: '1:912516069775:ios:ebb826a46e4933dfdfb3fa',
    messagingSenderId: '912516069775',
    projectId: 'the-ksquare-group',
    storageBucket: 'the-ksquare-group.appspot.com',
    iosBundleId: 'com.jixcayau.ksquare.RunnerTests',
  );
}
