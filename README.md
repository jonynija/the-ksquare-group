# Flutter Auth Sample

**Complete repository details:** [Repo](https://gitlab.com/jonynija/the-ksquare-group)

## Current Version Details

-   Flutter Version: 3.13.6
-   Integrated Platforms: Android, iOS, macOS, Web

## Recommended Versions

For optimal performance, it is recommended to use the following versions:

-   Flutter: 3.13.6
-   Xcode: 15
-   Android Studio: Flamingo

## Getting Started

To set up the project, follow these steps:

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/jonynija/the-ksquare-group.git
    ```

2. Navigate to the project directory:

    ```bash
    cd project
    ```

3. Run: `flutter pub get` to install dependencies.

For iOS and macOS, install CocoaPods:

-   Navigate to the `ios` or `macos` directory.
-   Run: `pod install --repo-update`

## Usage

-   Open the project in your preferred IDE (e.g., Visual Studio Code, IntelliJ).
-   Ensure the Flutter and Dart plugins/extensions are installed.
-   Run the app on your desired platform using the IDE's run command or `flutter run`.

## Example

Web: https://the-ksquare-group.web.app

# Developer

This project is maintained by [Jonathan Ixcayau](https://www.linkedin.com/in/jonathanixcayau/).

If you encounter any issues or have suggestions for improvement, feel free to open an [issue](https://gitlab.com/jonynija/the-ksquare-group/-/issues).

Enjoy coding with Flutter!
